package gmoldes.domain.interface_pattern;

import java.util.List;

public interface GenericAlphabetization <T, V> {

    List<T> alphabetization(List<V> entity);
}
