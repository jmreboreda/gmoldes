package gmoldes.domain.interface_pattern;

import java.util.List;

public interface GenericWithOutDuplicatesList <T, V> {

    List<V> withOutDuplicatesList(List<T> entity);
}
