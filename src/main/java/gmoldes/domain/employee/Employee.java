package gmoldes.domain.employee;

import gmoldes.domain.contract.Contract;
import gmoldes.domain.person.Person;

import java.time.LocalDate;
import java.util.Set;

public class Employee extends Person {

    private Set<Contract> contracts;

    private Employee(){
        }

    public Employee(Set<Contract> contracts) {
        this.contracts = contracts;
    }

    public Set<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(Set<Contract> contracts) {
        this.contracts = contracts;
    }

    public Boolean isWorkingAtDate(LocalDate date){
        Boolean isWorkingAtDate = false;

        for(Contract contract : contracts){
            if(isDateInDurationOfContract(contract, date)){
                isWorkingAtDate = true;
            }
        }

        return isWorkingAtDate;
    }

    private Boolean isDateInDurationOfContract(Contract contract, LocalDate date){

        if(date.compareTo(contract.getStartDate()) == 0 ||
                date.compareTo(contract.getEndingDate()) == 0){

            return true;
        }

        if(date.isAfter(contract.getStartDate()) &&
                date.isBefore(contract.getEndingDate())){

            return true;
        }

        return false;
    }
}
