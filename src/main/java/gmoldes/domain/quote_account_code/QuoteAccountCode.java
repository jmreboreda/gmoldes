package gmoldes.domain.quote_account_code;

import java.util.regex.Pattern;

public class QuoteAccountCode {

    private QuoteAccountCodeRegimeEnum regime;
    private String province;
    private String quoteAccountNumber;
    private String controlDigit;

    public QuoteAccountCode() {
    }

    public QuoteAccountCode(QuoteAccountCodeRegimeEnum regime, String quoteAccountCode) {
        this.regime = regime;
        this.province = quoteAccountCode.substring(0, 2);
        this.quoteAccountNumber = quoteAccountCode.substring(2, 9);
        this.controlDigit = quoteAccountCode.substring(9);
    }

    public QuoteAccountCode(QuoteAccountCodeRegimeEnum regime, String province, String quoteAccountNumber, String controlDigit){
        this.regime = regime;
        this.province = province;
        this.quoteAccountNumber = quoteAccountNumber;
        this.controlDigit = controlDigit;
    }

    public QuoteAccountCodeRegimeEnum getRegime() {
        return regime;
    }

    public String getRegimeDescription(){
        return regime.getQuoteAccountCodeRegimeDescription();
    }

    public String getRegimeCode(){
        return regime.getQuoteAccountCodeRegimeCode();
    }

    public void setRegime(QuoteAccountCodeRegimeEnum regime) {
        this.regime = regime;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getQuoteAccountNumber() {
        return quoteAccountNumber;
    }

    public void setQuoteAccountNumber(String quoteAccountNumber) {
        this.quoteAccountNumber = quoteAccountNumber;
    }

    public String getControlDigit() {
        return controlDigit;
    }

    public void setControlDigit(String controlDigit) {
        this.controlDigit = controlDigit;
    }

    public String toString(){

        return getRegime().getQuoteAccountCodeRegimeCode().concat("-").concat(getProvince()).concat(getQuoteAccountNumber()).concat(getControlDigit());
    }

    public QuoteAccountCodeRegimeEnum getQuoteAccountCodeEnumByDescription(String quoteAccountCodeDescription){
        return QuoteAccountCodeRegimeEnum.valueOf(quoteAccountCodeDescription);
    }

    public QuoteAccountCodeRegimeEnum getQuoteAccountCodeEnumByCode(String quoteAccountCode){
        QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnum = null;
        for (QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnumRead : QuoteAccountCodeRegimeEnum.values()){
            if(quoteAccountCodeRegimeEnumRead.getQuoteAccountCodeRegimeCode().equals(quoteAccountCode)){
                quoteAccountCodeRegimeEnum = quoteAccountCodeRegimeEnumRead;
            }
        }
        return quoteAccountCodeRegimeEnum;
    }

    public Boolean isValid(){

        Pattern provincePattern = Pattern.compile("\\d{2}");
        Pattern numbersPattern = Pattern.compile("\\d{7}");
        Pattern digitControlPattern = Pattern.compile("\\d{2}");

        if(!provincePattern.matcher(getProvince()).matches() ||
                !numbersPattern.matcher(getQuoteAccountNumber()).matches() ||
                !digitControlPattern.matcher(getControlDigit()).matches()){

            return false;
        }

        Long introducedProvince = Long.parseLong(getProvince());
        Long introducedQuoteAccountNumber = Long.parseLong(getQuoteAccountNumber());

        Long calculatedControlDigit;
        String calculatedControlDigitAsString;

        if (introducedProvince < 1 || (introducedProvince > 53 && introducedProvince != 66)) {
            return false;
        }

        if (introducedQuoteAccountNumber < 10000000) {
            calculatedControlDigit = (introducedProvince * 10000000 + introducedQuoteAccountNumber) % 97;

        } else {
            Long numberNASSWithoutControlDigit = Long.parseLong(introducedProvince.toString().concat(introducedQuoteAccountNumber.toString()));
            calculatedControlDigit = numberNASSWithoutControlDigit % 97;
        }

        if (calculatedControlDigit <= 9){
                calculatedControlDigitAsString = "0".concat(calculatedControlDigit.toString());
        }else{
            calculatedControlDigitAsString = calculatedControlDigit.toString();
        }

        if(calculatedControlDigitAsString.equals(getControlDigit())){
            return true;
        }

        return false;
    }
}
