package gmoldes.domain.quote_account_code.persistence.vo;

import gmoldes.domain.client.persistence.vo.ClientVO;
import gmoldes.domain.quote_account_code.QuoteAccountCodeRegimeEnum;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "quote_account_code")
@NamedQueries(value = {
        @NamedQuery(
                name = QuoteAccountCodeVO.FIND_ALL_QUOTE_ACCOUNT_CODE_BY_CLIENT_ID,
                query = "select p from QuoteAccountCodeVO p where clientId = :code"
        )
})

public class QuoteAccountCodeVO implements Serializable{

    public static final String FIND_ALL_QUOTE_ACCOUNT_CODE_BY_CLIENT_ID = "QuoteAccountCodeVO.FIND_ALL_QUOTE_ACCOUNT_CODE_BY_CLIENT_ID";

    @Id
    @SequenceGenerator(name = "quote_account_code_id_seq", sequenceName = "quote_account_code_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "quote_account_code_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private String quoteAccountCode;
    private String regime;
    @Column(length = 2)
    private String province;
    private String quoteAccountNumber;
    private String controlDigit;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="clientId")
    private ClientVO clientVO;

    public QuoteAccountCodeVO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuoteAccountCode() {
        return quoteAccountCode;
    }

    public void setQuoteAccountCode(String quoteAccountCode) {
        this.quoteAccountCode = quoteAccountCode;
    }

    public String getRegime() {
        return regime;
    }

    public void setRegime(String regime) {
        this.regime = regime;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getQuoteAccountNumber() {
        return quoteAccountNumber;
    }

    public void setQuoteAccountNumber(String quoteAccountNumber) {
        this.quoteAccountNumber = quoteAccountNumber;
    }

    public String getControlDigit() {
        return controlDigit;
    }

    public void setControlDigit(String controlDigit) {
        this.controlDigit = controlDigit;
    }

    public ClientVO getClientVO() {
        return clientVO;
    }

    public void setClientVO(ClientVO clientVO) {
        this.clientVO = clientVO;
    }

    public QuoteAccountCodeRegimeEnum getQuoteAccountCodeRegimeEnum(){
        QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnum = null;
        for(QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnumRead : QuoteAccountCodeRegimeEnum.values()){
            if(quoteAccountCodeRegimeEnumRead.getQuoteAccountCodeRegimeCode().equals(getRegime())){
                quoteAccountCodeRegimeEnum = quoteAccountCodeRegimeEnumRead;
            }
        }

        return quoteAccountCodeRegimeEnum;
    }
}
