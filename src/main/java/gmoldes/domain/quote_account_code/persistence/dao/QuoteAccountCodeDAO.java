package gmoldes.domain.quote_account_code.persistence.dao;


import gmoldes.domain.quote_account_code.persistence.vo.QuoteAccountCodeVO;
import gmoldes.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.persistence.Query;
import java.util.List;

public class QuoteAccountCodeDAO {

    private SessionFactory sessionFactory;
    private Session session;

    public QuoteAccountCodeDAO() {
    }

     public static class QuoteAccountCodeDAOFactory {

        private static QuoteAccountCodeDAO quoteAccountCodeDAO;

        public static QuoteAccountCodeDAO getInstance() {
            if(quoteAccountCodeDAO == null) {
                quoteAccountCodeDAO = new QuoteAccountCodeDAO(HibernateUtil.retrieveGlobalSession());
            }
            return quoteAccountCodeDAO;
        }
    }

    public QuoteAccountCodeDAO(Session session) {
        this.session = session;
    }

    public List<QuoteAccountCodeVO> findAllCCCByClientId(Integer id){

        Query query = session.createNamedQuery(QuoteAccountCodeVO.FIND_ALL_QUOTE_ACCOUNT_CODE_BY_CLIENT_ID, QuoteAccountCodeVO.class);
        query.setParameter("code", id);

        return (List<QuoteAccountCodeVO>) query.getResultList();
    }
}
