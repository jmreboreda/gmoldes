package gmoldes.domain.quote_account_code.dto;

public class QuoteAccountCodeDTO {

    private Integer id;
    private Integer clientId;
    private String quoteAccountCode;

    public QuoteAccountCodeDTO(Integer id, Integer clientId, String quoteAccountCode) {
        this.id = id;
        this.clientId = clientId;
        this.quoteAccountCode = quoteAccountCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getQuoteAccountCode() {
        return quoteAccountCode;
    }

    public void setQuoteAccountCode(String quoteAccountCode) {
        this.quoteAccountCode = quoteAccountCode;
    }

    public String toString(){
        return quoteAccountCode;
    }

    public static QuoteAccountCodeDTOBuilder create() {
        return new QuoteAccountCodeDTOBuilder();
    }

    public static class QuoteAccountCodeDTOBuilder {

        private Integer id;
        private Integer clientId;
        private String quoteAccountCode;

        public QuoteAccountCodeDTOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public QuoteAccountCodeDTOBuilder withClientId(Integer clientId) {
            this.clientId = clientId;
            return this;
        }

        public QuoteAccountCodeDTOBuilder withQuoteAccountCode(String cccInss) {
            this.quoteAccountCode = cccInss;
            return this;
        }

        public QuoteAccountCodeDTO build() {
            return new QuoteAccountCodeDTO(this.id, this.clientId, this.quoteAccountCode);
        }
    }
}
