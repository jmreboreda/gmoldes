package gmoldes.domain.quote_account_code;

public enum QuoteAccountCodeRegimeEnum {

    GENERAL_REGIME("Régimen general", "0111"),
    SELF_EMPLOYED_REGIME("Régimen especial de trabajadores por cuenta propia o autónomos", "XXXX"),
    ARTISTS_REGIME("Colectivo de artistas", "0112"),
    FRUITS_AND_OTHERS_REGIME("Sistema especial de frutas, hortalizas y conservas vegetales", "0132"),
    FRESH_TOMATO_MANIPULATION_REGIME("Sistema especial de empaquetado y manipulado del tomate Fresco", "0134"),
    FIXED_DISCONTINUOUS_IN_CINEMAS_AND_DISCOS_REGIME("Sistema especial de los trabajadores fijos discontinuos que prestan sus servicios en las empresas de exhibición cinematográfica, salas de baile, discotecas y salas de fiesta", "0136"),
    FIXED_DISCONTINUOUS_IN_MARKET_RESEARCH_AND_PUBLIC_OPINION_COMPANIES_REGIME("Sistema especial de los trabajadores fijos discontinuos que prestan sus servicios en empresas de estudio de mercado y opinión pública", "0137"),
    AGRARIAN_REGIME("Sistema especial agrario", "0163"),
    COAL_MINING_REGIME("Sistema especial de minería del carbón", "XXXX"),
    MARITIME_WORKERS_WITH_LAND_OCCUPATION_REGIME("Régimen de los trabajadores del mar con ocupación en tierra", "0811"),
    MARITIME_WORKERS_WITH_SEA_OCCUPATION_REGIME("Régimen de los trabajadores del mar con ocupación en la mar", "0814");

    private String quoteAccountCodeRegimeDescription;
    private String quoteAccountCodeRegimeCode;

    QuoteAccountCodeRegimeEnum(String quoteAccountCodeRegimeDescription, String quoteAccountCodeRegimeCode) {
        this.quoteAccountCodeRegimeDescription = quoteAccountCodeRegimeDescription;
        this.quoteAccountCodeRegimeCode = quoteAccountCodeRegimeCode;
    }

    public String getQuoteAccountCodeRegimeDescription() {
        return quoteAccountCodeRegimeDescription;
    }

    public String getQuoteAccountCodeRegimeCode() {
        return quoteAccountCodeRegimeCode;
    }

    public String toString(){
        return getQuoteAccountCodeRegimeCode().concat("-").concat(getQuoteAccountCodeRegimeDescription());
    }
}
