package gmoldes.domain.quote_account_code;


public class QuoteAccountCodeRegimeLocator {

    private String socialSecurityRegimeDescription;
    private String socialSecurityRegimeCode;

    public String getSocialSecurityRegimeDescriptionLocator(String socialSecurityRegimeCode) {

        QuoteAccountCodeRegimeEnum[] quoteAccountCodeRegimeEnumMatrices = QuoteAccountCodeRegimeEnum.values();
        for (QuoteAccountCodeRegimeEnum socialSecurityRegimenEnumElement : quoteAccountCodeRegimeEnumMatrices) {
            if (socialSecurityRegimenEnumElement.getQuoteAccountCodeRegimeCode().equals(socialSecurityRegimeCode)) {
                socialSecurityRegimeDescription = socialSecurityRegimenEnumElement.getQuoteAccountCodeRegimeDescription();
            }
        }

        return socialSecurityRegimeDescription;
    }

    public String getSocialSecurityRegimeCode(String socialSecurityRegimeDescription) {

        QuoteAccountCodeRegimeEnum[] quoteAccountCodeRegimeEnumMatrices = QuoteAccountCodeRegimeEnum.values();
        for (QuoteAccountCodeRegimeEnum socialSecurityRegimenEnumElement : quoteAccountCodeRegimeEnumMatrices) {
            if (socialSecurityRegimenEnumElement.getQuoteAccountCodeRegimeDescription().equals(socialSecurityRegimeDescription)) {
                socialSecurityRegimeCode = socialSecurityRegimenEnumElement.getQuoteAccountCodeRegimeCode();
            }
        }

        return socialSecurityRegimeCode;
    }
}
