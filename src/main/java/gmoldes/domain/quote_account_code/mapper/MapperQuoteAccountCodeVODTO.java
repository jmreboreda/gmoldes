package gmoldes.domain.quote_account_code.mapper;

import gmoldes.domain.quote_account_code.dto.QuoteAccountCodeDTO;
import gmoldes.domain.quote_account_code.persistence.vo.QuoteAccountCodeVO;

public class MapperQuoteAccountCodeVODTO {

    public static QuoteAccountCodeDTO map(QuoteAccountCodeVO quoteAccountCodeVO) {

            return QuoteAccountCodeDTO.create()
                    .withId(quoteAccountCodeVO.getId())
                    .withClientId(quoteAccountCodeVO.getClientVO().getClientId())
                    .withQuoteAccountCode(quoteAccountCodeVO.getQuoteAccountCode())
                    .build();
    }
}
