package gmoldes.domain.servicegm;

public enum ServiceGMEnum {

    RENTS("Alquileres", false,10),
    ADVISORY_WITH_INVOICES("Asesoría", true,20),
    ADVISORY_MODULES_WITHOUT_INVOICES_EXCEPT_FOR_AEAT_309("Asesoría - Módulos", true, 30),
    ADVISORY_MODULES_WITH_INVOICES("Asesoría - Módulos - IVA", true, 40);

    private String serviceGMDescription;
    private Boolean claimInvoices;
    private Integer serviceGMCode;

    ServiceGMEnum(String serviceGMDescription, Boolean claimInvoices, Integer serviceGMCode) {
        this.serviceGMDescription = serviceGMDescription;
        this.claimInvoices = claimInvoices;
        this.serviceGMCode = serviceGMCode;
    }

    public String getServiceGMDescription() {
        return serviceGMDescription;
    }

    public Boolean getClaimInvoices() {
        return claimInvoices;
    }

    public Integer getServiceGMCode() {
        return serviceGMCode;
    }

    public String toString(){
        return getServiceGMDescription();
    }
}
