package gmoldes.domain.social_security_affiliation_number;

import java.util.regex.Pattern;

public class SocialSecurityAffiliationNumber {

    private String province;
    private String affiliationNumber;
    private String controlDigit;

    public SocialSecurityAffiliationNumber() {
    }

    public SocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
        this.province = socialSecurityAffiliationNumber.substring(0, 2);
        this.affiliationNumber = socialSecurityAffiliationNumber.substring(2, 10);
        this.controlDigit = socialSecurityAffiliationNumber.substring(10);
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getAffiliationNumber() {
        return affiliationNumber;
    }

    public void setAffiliationNumber(String affiliationNumber) {
        this.affiliationNumber = affiliationNumber;
    }

    public String getControlDigit() {
        return controlDigit;
    }

    public void setControlDigit(String controlDigit) {
        this.controlDigit = controlDigit;
    }

    public String toString(){

        return getProvince().concat(getAffiliationNumber()).concat(getControlDigit());
    }

    public Boolean isValid(){

        Pattern provincePattern = Pattern.compile("\\d{2}");
        Pattern numbersPattern = Pattern.compile("\\d{8}");
        Pattern digitControlPattern = Pattern.compile("\\d{2}");

        if(!provincePattern.matcher(getProvince()).matches() ||
                !numbersPattern.matcher(getAffiliationNumber()).matches() ||
                !digitControlPattern.matcher(getControlDigit()).matches()){

            return false;
        }

        Long introducedProvince = Long.parseLong(getProvince());
        Long introducedAffiliationNumber = Long.parseLong(getAffiliationNumber());

        Long calculatedControlDigit;
        String calculatedControlDigitAsString;

        if (introducedProvince < 1 || (introducedProvince > 53 && introducedProvince != 66)) {
            return false;
        }

        if (introducedAffiliationNumber < 10000000) {
            calculatedControlDigit = (introducedProvince * 10000000 + introducedAffiliationNumber) % 97;

        } else {
            Long numberNASSWithoutControlDigit = Long.parseLong(introducedProvince.toString().concat(introducedAffiliationNumber.toString()));
            calculatedControlDigit = numberNASSWithoutControlDigit % 97;
        }

        if (calculatedControlDigit <= 9){
            calculatedControlDigitAsString = "0".concat(calculatedControlDigit.toString());
        }else{
            calculatedControlDigitAsString = calculatedControlDigit.toString();
        }

        if(calculatedControlDigitAsString.equals(getControlDigit())){
            return true;
        }

        return false;
    }
}
