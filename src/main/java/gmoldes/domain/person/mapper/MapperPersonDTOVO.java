package gmoldes.domain.person.mapper;

import gmoldes.domain.person.dto.PersonDTO;
import gmoldes.domain.person.persistence.vo.PersonVO;

import java.sql.Date;

public class MapperPersonDTOVO {


    public static PersonVO map(PersonDTO personDTO) {

        Date fechaNacim = personDTO.getBirthDate() != null ? Date.valueOf(personDTO.getBirthDate()): null;

        PersonVO personVO = new PersonVO();

        personVO.setIdpersona(personDTO.getPersonId());
        personVO.setApellidos(personDTO.getSurnames());
        personVO.setNom_rzsoc(personDTO.getName_LegalName());
        personVO.setNifcif(personDTO.getNieNif());
        personVO.setNifcifdup(personDTO.getNieNifDup());
        personVO.setNumafss(personDTO.getSocialSecurityAffiliationNumber());
        personVO.setFechanacim(fechaNacim);
        personVO.setEstciv(personDTO.getCivilStatus());
        personVO.setDireccion(personDTO.getAddress());
        personVO.setLocalidad(personDTO.getLocation());
        personVO.setCodpostal(personDTO.getPostalCode());
        personVO.setNivestud(personDTO.getStudyLevel());
        personVO.setNacionalidad(personDTO.getNationality());

        return personVO;
    }
}
