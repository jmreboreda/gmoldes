package gmoldes.domain.person.mapper;

import gmoldes.domain.person.dto.PersonDTO;
import gmoldes.domain.person.persistence.vo.PersonVO;

import java.time.LocalDate;

public class MapperPersonVODTO {


    public static PersonDTO mapPersonVODTO(PersonVO personVO){

        LocalDate birthDate = personVO.getFechanacim() != null ? personVO.getFechanacim().toLocalDate() : null;

        return PersonDTO.create()
                .withPersonId(personVO.getIdpersona())
                .withSurnames(personVO.getApellidos())
                .withName_LegalName(personVO.getNom_rzsoc())
                .withPostalCode(personVO.getCodpostal())
                .withAddress(personVO.getDireccion())
                .withCivilStatus(personVO.getEstciv())
                .withSocialSecurityAffiliationNumber(personVO.getNumafss())
                .withBirthDate(birthDate)
                .withLocation(personVO.getLocalidad())
                .withNationality(personVO.getNacionalidad())
                .withNieNif(personVO.getNifcif())
                .withNieNifDup(personVO.getNifcifdup())
                .withStudyLevel(personVO.getNivestud())
                .build();
    }
}
