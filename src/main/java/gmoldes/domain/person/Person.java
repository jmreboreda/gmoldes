package gmoldes.domain.person;

import gmoldes.domain.address.Address;
import gmoldes.domain.social_security_affiliation_number.SocialSecurityAffiliationNumber;
import gmoldes.domain.study.StudyLevelEnum;

import java.time.LocalDate;
import java.util.Set;

public class Person {

    private Integer personId;
    private String firstSurname;
    private String secondSurname;
    private String name;
    private String nieNif;
    private SocialSecurityAffiliationNumber socialSecurityAffiliationNumber;
    private LocalDate birthDate;
    private String civilStatus;
    private Set<Address> addresses;
    private StudyLevelEnum studyLevel;
    private String nationality;

    public Person() {
    }

    public Person(Integer personId,
                  String firstSurname,
                  String secondSurname,
                  String name,
                  String nieNif,
                  SocialSecurityAffiliationNumber socialSecurityAffiliationNumber,
                  LocalDate birthDate,
                  String civilStatus,
                  Set<Address> addresses,
                  StudyLevelEnum studyLevel,
                  String nationality) {

        this.personId = personId;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.name = name;
        this.nieNif = nieNif;
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
        this.birthDate = birthDate;
        this.civilStatus = civilStatus;
        this.addresses = addresses;
        this.studyLevel = studyLevel;
        this.nationality = nationality;
    }

    public Integer getPersonId() {
        return personId;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public String getName() {
        return name;
    }

    public String getNieNif() {
        return nieNif;
    }

    public SocialSecurityAffiliationNumber getSocialSecurityAffiliationNumber() {
        return socialSecurityAffiliationNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public StudyLevelEnum getStudyLevel() {
        return studyLevel;
    }

    public String getNationality() {
        return nationality;
    }

    @Override
    public String toString() {
        return toAlphabeticalName();
    }

    public String toAlphabeticalName(){
            return getFirstSurname().concat(" ").concat(getSecondSurname()).concat(", ") + getName();
    }

    public String toNaturalName(){

        return getName().concat(" ").concat(getFirstSurname()).concat(" ").concat(getSecondSurname());
    }

    public static Person.PersonBuilder create() {
        return new Person.PersonBuilder();
    }

    public static class PersonBuilder {

        private Integer personId;
        private String firstSurname;
        private String secondSurname;
        private String name;
        private String nifNie;
        private SocialSecurityAffiliationNumber socialSecurityAffiliationNumber;
        private LocalDate birthDate;
        private String civilState;
        private Set<Address> addresses;
        private StudyLevelEnum studyLevel;
        private String nationality;

        public Person.PersonBuilder withPersonId(Integer personId) {
            this.personId = personId;
            return this;
        }

        public Person.PersonBuilder withFirstSurname(String firstSurname) {
            this.firstSurname = firstSurname;
            return this;
        }

        public Person.PersonBuilder withSecondSurname(String secondSurname) {
            this.secondSurname = secondSurname;
            return this;
        }

        public Person.PersonBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public Person.PersonBuilder withNifNie(String nifNie) {
            this.nifNie = nifNie;
            return this;
        }

        public Person.PersonBuilder withSocialSecurityAffiliationNumber(SocialSecurityAffiliationNumber socialSecurityAffiliationNumber) {
            this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
            return this;
        }

        public Person.PersonBuilder withBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Person.PersonBuilder withCivilState(String civilState) {
            this.civilState = civilState;
            return this;
        }

        public Person.PersonBuilder withAddresses(Set<Address> addresses) {
            this.addresses = addresses;
            return this;
        }

        public Person.PersonBuilder withStudyLevel(StudyLevelEnum studyLevel) {
            this.studyLevel = studyLevel;
            return this;
        }

        public Person.PersonBuilder withNationality(String nationality) {
            this.nationality = nationality;
            return this;
        }

        public Person build() {
            return new Person(this.personId, this.firstSurname, this.secondSurname, this.name, this.nifNie, this.socialSecurityAffiliationNumber, this.birthDate, this.civilState,
                    this.addresses, this.studyLevel, this.nationality);
        }
    }
}