package gmoldes.domain.person.utilities;

import gmoldes.domain.interface_pattern.GenericAlphabetization;
import gmoldes.domain.person.dto.PersonDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class PersonAlphabetization implements GenericAlphabetization<PersonDTO, PersonDTO> {

    public ObservableList<PersonDTO> alphabetization(List<PersonDTO> personDTOList){

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<PersonDTO> sortedClientDTOList = personDTOList
                .stream()
                .sorted(Comparator.comparing(PersonDTO::toString, primaryCollator)).collect(Collectors.toList());

        return FXCollections.observableArrayList(sortedClientDTOList);
    }
}
