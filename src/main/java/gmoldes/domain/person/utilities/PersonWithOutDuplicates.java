package gmoldes.domain.person.utilities;

import gmoldes.domain.interface_pattern.GenericWithOutDuplicatesList;
import gmoldes.domain.person.dto.PersonDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonWithOutDuplicates implements GenericWithOutDuplicatesList<PersonDTO, PersonDTO> {

    @Override
    public List<PersonDTO> withOutDuplicatesList(List<PersonDTO> personDTOList) {
        Map<Integer, PersonDTO> personDTOMap = new HashMap<>();

        for (PersonDTO personDTO : personDTOList) {
            personDTOMap.put(personDTO.getPersonId(), personDTO);
        }

        List<PersonDTO> withoutDuplicatesPersonDTOList = new ArrayList<>();
        for (Map.Entry<Integer, PersonDTO> itemMap : personDTOMap.entrySet()) {
            withoutDuplicatesPersonDTOList.add(itemMap.getValue());
        }

        return withoutDuplicatesPersonDTOList;
    }
}
