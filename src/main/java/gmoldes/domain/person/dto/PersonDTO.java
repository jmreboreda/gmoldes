/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmoldes.domain.person.dto;

import java.math.BigDecimal;
import java.time.LocalDate;


public class PersonDTO {

    private Integer personId;
    private String surnames;
    private String name_LegalName;
    private String nieNif;
    private Short nieNifDup;
    private String socialSecurityAffiliationNumber;
    private LocalDate birthDate;
    private String civilStatus;
    private String address;
    private String location;
    private BigDecimal postalCode;
    private Integer studyLevel;
    private String nationality;

    public PersonDTO(Integer personId, String surnames, String name_LegalName, String nieNif, Short nieNifDup,
                     String socialSecurityAffiliationNumber, LocalDate birthDate, String civilStatus, String address, String location,
                     BigDecimal postalCode, Integer studyLevel, String nationality) {
        this.personId = personId;
        this.surnames = surnames;
        this.name_LegalName = name_LegalName;
        this.nieNif = nieNif;
        this.nieNifDup = nieNifDup;
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
        this.birthDate = birthDate;
        this.civilStatus = civilStatus;
        this.address = address;
        this.location = location;
        this.postalCode = postalCode;
        this.studyLevel = studyLevel;
        this.nationality = nationality;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getName_LegalName() {
        return name_LegalName;
    }

    public void setName_LegalName(String name_LegalName) {
        this.name_LegalName = name_LegalName;
    }

    public String getNieNif() {
        return nieNif;
    }

    public void setNieNif(String nieNif) {
        this.nieNif = nieNif;
    }

    public Short getNieNifDup() {
        return nieNifDup;
    }

    public void setNieNifDup(Short nieNifDup) {
        this.nieNifDup = nieNifDup;
    }

    public String getSocialSecurityAffiliationNumber() {
        return socialSecurityAffiliationNumber;
    }

    public void setSocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public BigDecimal getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(BigDecimal postalCode) {
        this.postalCode = postalCode;
    }

    public Integer getStudyLevel() {
        return studyLevel;
    }

    public void setStudyLevel(Integer studyLevel) {
        this.studyLevel = studyLevel;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return toAlphabeticalName();
    }

    public String toNaturalName(){

        return getName_LegalName() + " " + getSurnames();
    }

    public String toAlphabeticalName(){

        return getSurnames() + ", " + getName_LegalName();
    }

    public static PersonDTO.PersonBuilder create() {
        return new PersonDTO.PersonBuilder();
    }

    public static class PersonBuilder {

        private Integer personId;
        private String surnames;
        private String name_LegalName;
        private String nieNif;
        private Short nieNifDup;
        private String socialSecurityAffiliationNumber;
        private LocalDate birthDate;
        private String civilStatus;
        private String address;
        private String location;
        private BigDecimal postalCode;
        private Integer studyLevel;
        private String nationality;

        public PersonDTO.PersonBuilder withPersonId(Integer personId) {
            this.personId = personId;
            return this;
        }

        public PersonDTO.PersonBuilder withSurnames(String surnames) {
            this.surnames = surnames;
            return this;
        }

        public PersonDTO.PersonBuilder withName_LegalName(String legalName) {
            this.name_LegalName = legalName;
            return this;
        }

        public PersonDTO.PersonBuilder withNieNif(String nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public PersonDTO.PersonBuilder withNieNifDup(Short nieNifDup) {
            this.nieNifDup = nieNifDup;
            return this;
        }

        public PersonDTO.PersonBuilder withSocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
            this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
            return this;
        }

        public PersonDTO.PersonBuilder withBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public PersonDTO.PersonBuilder withCivilStatus(String civilStatus) {
            this.civilStatus = civilStatus;
            return this;
        }

        public PersonDTO.PersonBuilder withAddress(String address) {
            this.address = address;
            return this;
        }

        public PersonDTO.PersonBuilder withLocation(String location) {
            this.location = location;
            return this;
        }

        public PersonDTO.PersonBuilder withPostalCode(BigDecimal postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public PersonDTO.PersonBuilder withStudyLevel(Integer studyLevel) {
            this.studyLevel = studyLevel;
            return this;
        }

        public PersonDTO.PersonBuilder withNationality(String nationality) {
            this.nationality = nationality;
            return this;
        }

        public PersonDTO build() {
            return new PersonDTO(this.personId, this.surnames, this.name_LegalName, this.nieNif, this.nieNifDup, this.socialSecurityAffiliationNumber,
            this.birthDate, this.civilStatus, this.address, this.location, this.postalCode, this.studyLevel, this.nationality);
        }
    }
}