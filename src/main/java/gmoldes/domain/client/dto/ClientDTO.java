/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmoldes.domain.client.dto;

import gmoldes.domain.quote_account_code.persistence.vo.QuoteAccountCodeVO;
import gmoldes.domain.contract.persistence.vo.ContractVO;
import gmoldes.domain.servicegm.persistence.vo.ServiceGMVO;

import java.time.LocalDate;
import java.util.Set;


public class ClientDTO {

    private Integer id;
    private Integer clientId;
    private Boolean isNaturalPerson;
    private Boolean isLegalPerson;
    private Boolean isEntityWithoutLegalPersonality;
    private String nieNif;
    private String surNames;
    private String name;
    private String legalName;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String sg21Code;
    private Boolean activeClient;
    private LocalDate withoutActivity;
    private Set<ServiceGMVO> servicesGM;
    private Set<QuoteAccountCodeVO> quoteAccountCodes;
    private Set<ContractVO> contracts;

    public ClientDTO() {
    }

    public ClientDTO(
            Integer id,
            Integer clientId,
            Boolean isNaturalPerson,
            Boolean isLegalPerson,
            Boolean isEntityWithoutLegalPersonality,
            String nieNif,
            String surNames,
            String name,
            String legalName,
            LocalDate dateFrom,
            LocalDate dateTo,
            String sg21Code,
            Boolean activeClient,
            LocalDate withoutActivity,
            Set<ServiceGMVO> servicesGM,
            Set<QuoteAccountCodeVO> quoteAccountCodes,
            Set<ContractVO> contracts) {
        this.id = id;
        this.clientId = clientId;
        this.isNaturalPerson = isNaturalPerson;
        this.isLegalPerson = isLegalPerson;
        this.isEntityWithoutLegalPersonality = isEntityWithoutLegalPersonality;
        this.nieNif = nieNif;
        this.surNames = surNames;
        this.name = name;
        this.legalName = legalName;
        this.sg21Code = sg21Code;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.activeClient = activeClient;
        this.withoutActivity = withoutActivity;
        this.servicesGM = servicesGM;
        this.quoteAccountCodes = quoteAccountCodes;
        this.contracts = contracts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Boolean getNaturalPerson() {
        return isNaturalPerson;
    }

    public void setNaturalPerson(Boolean naturalPerson) {
        isNaturalPerson = naturalPerson;
    }

    public Boolean getLegalPerson() {
        return isLegalPerson;
    }

    public void setLegalPerson(Boolean legalPerson) {
        isLegalPerson = legalPerson;
    }

    public Boolean getEntityWithoutLegalPersonality() {
        return isEntityWithoutLegalPersonality;
    }

    public void setEntityWithoutLegalPersonality(Boolean entityWithoutLegalPersonality) {
        isEntityWithoutLegalPersonality = entityWithoutLegalPersonality;
    }

    public String getNieNif() {
        return nieNif;
    }

    public void setNieNif(String nieNif) {
        this.nieNif = nieNif;
    }

    public String getSurNames() {
        return surNames;
    }

    public void setSurNames(String surNames) {
        this.surNames = surNames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public void setSg21Code(String sg21Code) {
        this.sg21Code = sg21Code;
    }

    public Boolean getActiveClient() {
        return activeClient;
    }

    public void setActiveClient(Boolean activeClient) {
        this.activeClient = activeClient;
    }

    public LocalDate getWithoutActivity() {
        return withoutActivity;
    }

    public void setWithoutActivity(LocalDate withoutActivity) {
        this.withoutActivity = withoutActivity;
    }

    public Set<ServiceGMVO> getServicesGM() {
        return servicesGM;
    }

    public void setServicesGM(Set<ServiceGMVO> servicesGM) {
        this.servicesGM = servicesGM;
    }

    public Set<QuoteAccountCodeVO> getQuoteAccountCodes() {
        return quoteAccountCodes;
    }

    public void setQuoteAccountCodes(Set<QuoteAccountCodeVO> quoteAccountCodes) {
        this.quoteAccountCodes = quoteAccountCodes;
    }

    public Set<ContractVO> getContracts() {
        return contracts;
    }

    public void setContracts(Set<ContractVO> contracts) {
        this.contracts = contracts;
    }

    public String toString() {
        if(isNaturalPerson){
            return getSurNames() + ", " + getName();
        }

        return getLegalName();
    }

    public String toNaturalName(){
        if(isNaturalPerson){
            return getName() + " " + getSurNames();
        }

        return getLegalName();
    }

    public Boolean isActiveClient(){
        if(getDateTo() == null){
            return true;
        }

        return false;
    }

    public static ClientDTOOkBuilder create() {
        return new ClientDTOOkBuilder();
    }

    public static class ClientDTOOkBuilder {

        private Integer id;
        private Integer clientId;
        private Boolean isNaturalPerson;
        private Boolean isLegalPerson;
        private Boolean isEntityWithoutLegalPersonality;
        private String nieNif;
        private String surNames;
        private String name;
        private String legalName;
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private String sg21Code;
        private Boolean activeClient;
        private LocalDate withoutActivity;
        private Set<ServiceGMVO> servicesGM;
        private Set<QuoteAccountCodeVO> quoteAccountCodes;
        private Set<ContractVO> contracts;

        public ClientDTOOkBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public ClientDTOOkBuilder withClientId(Integer clientId) {
            this.clientId = clientId;
            return this;
        }

        public ClientDTOOkBuilder withIsNaturalPerson(Boolean isNaturalPerson) {
            this.isNaturalPerson = isNaturalPerson;
            return this;
        }

        public ClientDTOOkBuilder withIsLegalPerson(Boolean isLegalPerson) {
            this.isLegalPerson = isLegalPerson;
            return this;
        }

        public ClientDTOOkBuilder withIsEntityWithoutLegalPersonality(Boolean isEntityWithoutLegalPersonality) {
            this.isEntityWithoutLegalPersonality = isEntityWithoutLegalPersonality;
            return this;
        }

        public ClientDTOOkBuilder withNieNIF(String nieNIF) {
            this.nieNif = nieNIF;
            return this;
        }

        public ClientDTOOkBuilder withSurnames(String surNames) {
            this.surNames = surNames;
            return this;
        }

        public ClientDTOOkBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ClientDTOOkBuilder withLegalName(String legalName) {
            this.legalName = legalName;
            return this;
        }

        public ClientDTOOkBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ClientDTOOkBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ClientDTOOkBuilder withSg21Code(String sg21Code) {
            this.sg21Code = sg21Code;
            return this;
        }


        public ClientDTOOkBuilder withActiveClient(Boolean activeClient) {
            this.activeClient = activeClient;
            return this;
        }

        public ClientDTOOkBuilder withWithOutActivity(LocalDate withoutActivity) {
            this.withoutActivity = withoutActivity;
            return this;
        }

        public ClientDTOOkBuilder withServicesGM(Set<ServiceGMVO> serviceGMVOSet){
            this.servicesGM = serviceGMVOSet;
            return this;
        }

        public ClientDTOOkBuilder withQuoteAccountCodes(Set<QuoteAccountCodeVO> quoteAccountCodes){
            this.quoteAccountCodes = quoteAccountCodes;
            return this;
        }

        public ClientDTOOkBuilder withContracts(Set<ContractVO> contracts){
            this.contracts = contracts;
            return this;
        }

        public ClientDTO build() {
            return new ClientDTO(this.id, this.clientId, this.isNaturalPerson, this.isLegalPerson, this.isEntityWithoutLegalPersonality, this.nieNif, this.surNames, this.name,this.legalName, this.dateFrom, this.dateTo,
                    this.sg21Code, this.activeClient, this.withoutActivity, this.servicesGM, this.quoteAccountCodes, this.contracts);
        }
    }
}