package gmoldes.domain.client.utilities;

import gmoldes.domain.client.dto.ClientDTO;
import gmoldes.domain.interface_pattern.GenericAlphabetization;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ClientAlphabetization implements GenericAlphabetization<ClientDTO, ClientDTO> {

    public ObservableList<ClientDTO> alphabetization(List<ClientDTO> clientDTOList){

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<ClientDTO> sortedClientDTOList = clientDTOList
                .stream()
                .sorted(Comparator.comparing(ClientDTO::toString, primaryCollator)).collect(Collectors.toList());

        return FXCollections.observableArrayList(sortedClientDTOList);
    }
}
