package gmoldes.domain.client.utilities;

import gmoldes.domain.client.dto.ClientDTO;
import gmoldes.domain.interface_pattern.GenericWithOutDuplicatesList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientWithOutDuplicates implements GenericWithOutDuplicatesList<ClientDTO, ClientDTO> {

    @Override
    public List<ClientDTO> withOutDuplicatesList(List<ClientDTO> clientDTOList) {
        Map<Integer, ClientDTO> clientDTOMap = new HashMap<>();

        for (ClientDTO clientDTO : clientDTOList) {
            clientDTOMap.put(clientDTO.getClientId(), clientDTO);
        }

        List<ClientDTO> withoutDuplicatesClientDTOList = new ArrayList<>();
        for (Map.Entry<Integer, ClientDTO> itemMap : clientDTOMap.entrySet()) {
            withoutDuplicatesClientDTOList.add(itemMap.getValue());
        }

        return withoutDuplicatesClientDTOList;
    }
}
