/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmoldes.domain.client;

import gmoldes.domain.servicegm.ServiceGM;

import java.time.LocalDate;
import java.util.Set;


public class Client {

    private Integer clientId;
    private Boolean isNaturalPerson;
    private Boolean isLegalPerson;
    private Boolean isEntityWithoutLegalPersonality;
    private String nieNif;
    private String surNames;
    private String name;
    private String legalName;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String sg21Code;
    private Boolean activeClient;
    private LocalDate withoutActivity;
    private Set<ServiceGM> servicesGM;


    public Client() {
    }

    public Client(
            Integer clientId,
            Boolean isNaturalPerson,
            Boolean isLegalPerson,
            Boolean isEntityWithoutLegalPersonality,
            String nieNif,
            String surNames,
            String name,
            String legalName,
            LocalDate dateFrom,
            LocalDate dateTo,
            String sg21Code,
            Boolean activeClient,
            LocalDate withoutActivity,
            Set<ServiceGM> servicesGM) {
        this.clientId = clientId;
        this.isNaturalPerson = isNaturalPerson;
        this.isLegalPerson = isLegalPerson;
        this.isEntityWithoutLegalPersonality = isEntityWithoutLegalPersonality;
        this.nieNif = nieNif;
        this.surNames = surNames;
        this.name = name;
        this.legalName = legalName;
        this.sg21Code = sg21Code;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.activeClient = activeClient;
        this.withoutActivity = withoutActivity;
        this.servicesGM = servicesGM;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Boolean getNaturalPerson() {
        return isNaturalPerson;
    }

    public void setNaturalPerson(Boolean naturalPerson) {
        isNaturalPerson = naturalPerson;
    }

    public Boolean getLegalPerson() {
        return isLegalPerson;
    }

    public void setLegalPerson(Boolean legalPerson) {
        isLegalPerson = legalPerson;
    }

    public Boolean getEntityWithoutLegalPersonality() {
        return isEntityWithoutLegalPersonality;
    }

    public void setEntityWithoutLegalPersonality(Boolean entityWithoutLegalPersonality) {
        isEntityWithoutLegalPersonality = entityWithoutLegalPersonality;
    }

    public String getNieNif() {
        return nieNif;
    }

    public void setNieNif(String nieNif) {
        this.nieNif = nieNif;
    }

    public String getSurNames() {
        return surNames;
    }

    public void setSurNames(String surNames) {
        this.surNames = surNames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public void setSg21Code(String sg21Code) {
        this.sg21Code = sg21Code;
    }

    public Boolean getActiveClient() {
        return activeClient;
    }

    public void setActiveClient(Boolean activeClient) {
        this.activeClient = activeClient;
    }

    public LocalDate getWithoutActivity() {
        return withoutActivity;
    }

    public void setWithoutActivity(LocalDate withoutActivity) {
        this.withoutActivity = withoutActivity;
    }

    public Set<ServiceGM> getServicesGM() {
        return servicesGM;
    }

    public void setServicesGM(Set<ServiceGM> servicesGM) {
        this.servicesGM = servicesGM;
    }

    public String toString() {

        return getAlphabeticalOrderName();
    }

    public String toNaturalName(){
        if(isNaturalPerson){
            return getName() + " " + getSurNames();
        }

        return getLegalName();
    }

    public String getAlphabeticalOrderName(){
        if(isNaturalPerson){
            return getSurNames() + ", " + getName();
        }

        return getLegalName();
    }

    public static ClientBuilder create() {
        return new ClientBuilder();
    }

    public static class ClientBuilder {

        private Integer id;
        private Boolean isNaturalPerson;
        private Boolean isLegalPerson;
        private Boolean isEntityWithoutLegalPersonality;
        private String nieNif;
        private String surNames;
        private String name;
        private String legalName;
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private String sg21Code;
        private Boolean activeClient;
        private LocalDate withoutActivity;
        private Set<ServiceGM> servicesGM;

        public ClientBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public ClientBuilder withIsNaturalPerson(Boolean isNaturalPerson) {
            this.isNaturalPerson = isNaturalPerson;
            return this;
        }

        public ClientBuilder withIsLegalPerson(Boolean isLegalPerson) {
            this.isLegalPerson = isLegalPerson;
            return this;
        }

        public ClientBuilder withIsEntityWithoutLegalPersonality(Boolean isEntityWithoutLegalPersonality) {
            this.isEntityWithoutLegalPersonality = isEntityWithoutLegalPersonality;
            return this;
        }

        public ClientBuilder withNieNIF(String nieNIF) {
            this.nieNif = nieNIF;
            return this;
        }

        public ClientBuilder withSurnames(String surNames) {
            this.surNames = surNames;
            return this;
        }

        public ClientBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ClientBuilder withName_LegalName(String legalName) {
            this.legalName = legalName;
            return this;
        }

        public ClientBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ClientBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ClientBuilder withSg21Code(String sg21Code) {
            this.sg21Code = sg21Code;
            return this;
        }


        public ClientBuilder withActiveClient(Boolean activeClient) {
            this.activeClient = activeClient;
            return this;
        }

        public ClientBuilder withWithOutActivity(LocalDate withoutActivity) {
            this.withoutActivity = withoutActivity;
            return this;
        }

        public ClientBuilder withServicesGM(Set<ServiceGM> serviceGM){
            this.servicesGM = serviceGM;
            return this;
        }

        public Client build() {
            return new Client(this.id, this.isNaturalPerson, this.isLegalPerson, this.isEntityWithoutLegalPersonality, this.nieNif, this.surNames, this.name,this.legalName, this.dateFrom, this.dateTo,
                    this.sg21Code, this.activeClient, this.withoutActivity, this.servicesGM);
        }
    }
}