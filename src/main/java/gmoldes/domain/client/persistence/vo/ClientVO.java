package gmoldes.domain.client.persistence.vo;

import gmoldes.domain.quote_account_code.persistence.vo.QuoteAccountCodeVO;
import gmoldes.domain.servicegm.persistence.vo.ServiceGMVO;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

@Entity
//@Table(name = "client", uniqueConstraints = {@UniqueConstraint(columnNames = {"clientId", "clientType"})})
@Table(name = "client")
@NamedQueries(value = {
        @NamedQuery(
                name = ClientVO.FIND_ALL_ACTIVE_CLIENT,
                query = "select p from ClientVO as p where p.dateTo is null and withoutActivity is null"
        ),
        @NamedQuery(
                name = ClientVO.FIND_ALL_ACTIVE_CLIENTS_BY_NAME_PATTERN,
                query = "select p from ClientVO as p where (lower(p.legalName) like lower(:lettersOfName)" +
                        " or lower(p.surNames) like lower(:lettersOfName)" +
                        " or lower(p.name) like lower(:lettersOfName)" +
                        " or lower(concat(surNames, ', ', name)) like lower(:lettersOfName))" +
                        " and p.dateTo is null and withoutActivity is null"
        ),
        @NamedQuery(
                name = ClientVO.FIND_ALL_ACTIVE_CLIENTS_IN_ALPHABETICAL_ORDER,
                query = "select p from ClientVO as p where p.dateTo is null order by p.surNames, p.name, p.legalName"
        ),
        @NamedQuery(
                name = ClientVO.FIND_CLIENT_BY_CLIENT_ID,
                query = "select p from ClientVO as p where p.clientId = :clientId"
        ),
        @NamedQuery(
                name = ClientVO.FIND_ALL_CLIENT_WITH_INVOICES_TO_BE_REQUIRED_IN_PERIOD,
                query = "select p from ClientVO as p where (p.dateFrom is null or p.dateFrom <= :finalDate) and (p.dateTo is null or p.dateTo >= :finalDate) " +
                        "and p.withoutActivity is null and claimInvoices = true order by p.legalName, p.surNames, p.name"
        )
})

public class ClientVO implements Serializable {

    public static final String FIND_ALL_ACTIVE_CLIENT = "ClientVO.FIND_ALL_ACTIVE_CLIENT";
    public static final String FIND_ALL_ACTIVE_CLIENTS_BY_NAME_PATTERN = "ClientVO.FIND_ALL_ACTIVE_CLIENTS_BY_NAME_PATTERN";
    public static final String FIND_ALL_ACTIVE_CLIENTS_IN_ALPHABETICAL_ORDER = "ClientVO.FIND_ALL_ACTIVE_CLIENTS_IN_ALPHABETICAL_ORDER";
    public static final String FIND_CLIENT_BY_CLIENT_ID = "ClientVO.FIND_CLIENT_BY_CLIENT_ID";
    public static final String FIND_ALL_CLIENT_WITH_INVOICES_TO_BE_REQUIRED_IN_PERIOD = "ClientVO.FIND_ALL_CLIENT_WITH_INVOICES_TO_BE_REQUIRED_IN_PERIOD";

    @Id
    @SequenceGenerator(name = "client_id_seq", sequenceName = "client_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private Integer clientId;
    private Boolean isNaturalPerson;
    private Boolean isLegalPerson;
    private Boolean isEntityWithoutLegalPersonality;
    private String nieNif;
    private String surNames;
    private String name;
    private String legalName;
    private Date dateFrom;
    private Date dateTo;
    @Column(name = "sg21code", length = 4)
    private String sg21Code;
    private Date withoutActivity;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "clientVO", cascade = CascadeType.ALL)
    private Set<ServiceGMVO> servicesGM;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "clientVO", cascade = CascadeType.ALL)
    private Set<QuoteAccountCodeVO> quoteAccountCodes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Boolean getNaturalPerson() {
        return isNaturalPerson;
    }

    public void setNaturalPerson(Boolean naturalPerson) {
        isNaturalPerson = naturalPerson;
    }

    public Boolean getLegalPerson() {
        return isLegalPerson;
    }

    public void setLegalPerson(Boolean legalPerson) {
        isLegalPerson = legalPerson;
    }

    public Boolean getEntityWithoutLegalPersonality() {
        return isEntityWithoutLegalPersonality;
    }

    public void setEntityWithoutLegalPersonality(Boolean entityWithoutLegalPersonality) {
        isEntityWithoutLegalPersonality = entityWithoutLegalPersonality;
    }

    public String getNieNif() {
        return nieNif;
    }

    public void setNifCif(String nieNif) {
        this.nieNif = nieNif;
    }

    public String getSurNames() {
        return surNames;
    }

    public void setSurNames(String surNames) {
        this.surNames = surNames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public void setSg21Code(String sg21Code) {
        this.sg21Code = sg21Code;
    }

    public Date getWithoutActivity() {
        return withoutActivity;
    }

    public void setWithoutActivity(Date withoutActivity) {
        this.withoutActivity = withoutActivity;
    }

    public Set<ServiceGMVO> getServicesGM() {
        return servicesGM;
    }

    public void setServicesGM(Set<ServiceGMVO> servicesGM) {
        this.servicesGM = servicesGM;
    }

    public Set<QuoteAccountCodeVO> getQuoteAccountCodes() {
        return quoteAccountCodes;
    }

    public void setQuoteAccountCodes(Set<QuoteAccountCodeVO> quoteAccountCodes) {
        this.quoteAccountCodes = quoteAccountCodes;
    }

    public String toString(){
        if(isNaturalPerson){
            return getName() + " " + getSurNames();
        }

        return legalName;
    }

    public Boolean isActiveClient(){
        if(dateTo == null){
            return true;
        }

        return false;
    }

    public Boolean isActiveContributor(){
        if(withoutActivity == null){
            return true;
        }

        return false;
    }
}

