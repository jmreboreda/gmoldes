package gmoldes.domain.employer;

import gmoldes.domain.client.Client;
import gmoldes.domain.contract.Contract;
import gmoldes.domain.quote_account_code.QuoteAccountCode;
import gmoldes.domain.servicegm.ServiceGM;

import java.time.LocalDate;
import java.util.Set;

public class Employer extends Client {

    private Integer employerId;
    private Set<QuoteAccountCode> quoteAccountCodes;
    private Set<Contract> contracts;

    public Employer() {
    }

    public Employer(Set<QuoteAccountCode> quoteAccountCodes, Set<Contract> contracts) {
        this.employerId = getClientId();
        this.quoteAccountCodes = quoteAccountCodes;
        this.contracts = contracts;
    }

    public Integer getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Integer employerId) {
        this.employerId = employerId;
    }

    public Set<QuoteAccountCode> getQuoteAccountCodes() {
        return quoteAccountCodes;
    }

    public void setQuoteAccountCode(Set<QuoteAccountCode> quoteAccountCodes) {
        this.quoteAccountCodes = quoteAccountCodes;
    }

    public Set<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(Set<Contract> contracts) {
        this.contracts = contracts;
    }

    public String toString(){

        return getAlphabeticalOrderName();
    }

    @Override
    public Integer getClientId() {
        return super.getClientId();
    }

    @Override
    public void setClientId(Integer clientId) {
        super.setClientId(clientId);
    }

    @Override
    public Boolean getNaturalPerson() {
        return super.getNaturalPerson();
    }

    @Override
    public void setNaturalPerson(Boolean naturalPerson) {
        super.setNaturalPerson(naturalPerson);
    }

    @Override
    public Boolean getLegalPerson() {
        return super.getLegalPerson();
    }

    @Override
    public void setLegalPerson(Boolean legalPerson) {
        super.setLegalPerson(legalPerson);
    }

    @Override
    public Boolean getEntityWithoutLegalPersonality() {
        return super.getEntityWithoutLegalPersonality();
    }

    @Override
    public void setEntityWithoutLegalPersonality(Boolean entityWithoutLegalPersonality) {
        super.setEntityWithoutLegalPersonality(entityWithoutLegalPersonality);
    }

    @Override
    public String getNieNif() {
        return super.getNieNif();
    }

    @Override
    public void setNieNif(String nieNif) {
        super.setNieNif(nieNif);
    }

    @Override
    public String getSurNames() {
        return super.getSurNames();
    }

    @Override
    public void setSurNames(String surNames) {
        super.setSurNames(surNames);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getLegalName() {
        return super.getLegalName();
    }

    @Override
    public void setLegalName(String legalName) {
        super.setLegalName(legalName);
    }

    @Override
    public LocalDate getDateFrom() {
        return super.getDateFrom();
    }

    @Override
    public void setDateFrom(LocalDate dateFrom) {
        super.setDateFrom(dateFrom);
    }

    @Override
    public LocalDate getDateTo() {
        return super.getDateTo();
    }

    @Override
    public void setDateTo(LocalDate dateTo) {
        super.setDateTo(dateTo);
    }

    @Override
    public String getSg21Code() {
        return super.getSg21Code();
    }

    @Override
    public void setSg21Code(String sg21Code) {
        super.setSg21Code(sg21Code);
    }

    @Override
    public Boolean getActiveClient() {
        return super.getActiveClient();
    }

    @Override
    public void setActiveClient(Boolean activeClient) {
        super.setActiveClient(activeClient);
    }

    @Override
    public LocalDate getWithoutActivity() {
        return super.getWithoutActivity();
    }

    @Override
    public void setWithoutActivity(LocalDate withoutActivity) {
        super.setWithoutActivity(withoutActivity);
    }

    @Override
    public Set<ServiceGM> getServicesGM() {
        return super.getServicesGM();
    }

    @Override
    public void setServicesGM(Set<ServiceGM> servicesGM) {
        super.setServicesGM(servicesGM);
    }

    @Override
    public String toNaturalName() {
        return super.toNaturalName();
    }

    @Override
    public String getAlphabeticalOrderName() {
        return super.getAlphabeticalOrderName();
    }
}
