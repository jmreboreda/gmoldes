package gmoldes.domain.study;

public enum StudyLevelEnum {

    NOT_ESTABLISHED("No establecido", 0),
    WITHOUT_STUDIES("Sin estudios", 1),
    PRIMARY_STUDIES("Educación primaria", 32),
    EGB_STUDIES("Graduado escolar - E. G. B.", 2),
    ESO_STUDIES("E. S. O.", 3),
    COU_STUDIES("C. O. U.", 33),
    PROFESSIONAL_CERTIFICATE("Certificado de profesionalidad", 31),
    PROFESSIONAL_FORMATION_I("Formación profesional  I", 4),
    PROFESSIONAL_FORMATION_II("Formación profesional  II", 5),
    BACHELORS_DEGREE("Bachillerato", 6),
    MID_CYCLE("Ciclo medio", 7),
    SUPERIOR_CYCLE("Ciclo superior", 8),
    THREE_YEARS_UNIVERSITY_DEGREE("Diplomatura universitaria", 9),
    FOUR_YEARS_UNIVERSITY_DEGREE("Grado universitario", 10),
    FIVE_YEARS_UNIVERSITY_DEGREE("Licenciatura universitaria", 11);

    private String studyLevelDescription;
    private Integer studyLevelCode;

    StudyLevelEnum(String studyLevelDescription, Integer studyLevelCode) {
        this.studyLevelDescription = studyLevelDescription;
        this.studyLevelCode = studyLevelCode;
    }

    public String getStudyLevelDescription() {
        return studyLevelDescription;
    }

    public Integer getStudyLevelCode() {
        return studyLevelCode;
    }

    public String toString(){
        return getStudyLevelDescription();
    }
}
