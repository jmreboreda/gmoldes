package gmoldes.domain.study;

public class Study {

    public String getStudyLevelDescription(Integer studyLevelCode) {

        String studyDescription = null;

        StudyLevelEnum[] studyLevelEnumMatrix = StudyLevelEnum.values();
        for (StudyLevelEnum studyLevelEnumElement : studyLevelEnumMatrix) {
            if (studyLevelEnumElement.getStudyLevelCode().equals(studyLevelCode)) {
                studyDescription = studyLevelEnumElement.getStudyLevelDescription();
            }
        }

        return studyDescription;
    }

    public Integer getStudyLevelCode(String studyLevelDescription) {

        Integer studyLevelCode = null;

        StudyLevelEnum[] studyLevelEnumMatrix = StudyLevelEnum.values();
        for (StudyLevelEnum studyLevelEnumElement : studyLevelEnumMatrix) {
            if (studyLevelEnumElement.getStudyLevelDescription().equals(studyLevelDescription)) {
                studyLevelCode = studyLevelEnumElement.getStudyLevelCode();
            }
        }

        return studyLevelCode;
    }
}
