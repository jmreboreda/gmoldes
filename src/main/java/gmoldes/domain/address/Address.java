package gmoldes.domain.address;

public class Address {

    private String streetType;
    private String streetName;
    private String streetExtendedData;
    private String postalCode;
    private String location;
    private String municipality;
    private String province;

    public Address() {
    }

    public Address(String streetType, String streetName, String streetExtendedData, String postalCode, String location, String municipality, String province) {
        this.streetType = streetType;
        this.streetName = streetName;
        this.streetExtendedData = streetExtendedData;
        this.postalCode = postalCode;
        this.location = location;
        this.municipality = municipality;
        this.province = province;
    }

    public String getStreetType() {
        return streetType;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getStreetExtendedData() {
        return streetExtendedData;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getLocation() {
        return location;
    }

    public String getMunicipality() {
        return municipality;
    }

    public String getProvince() {
        return province;
    }

    public static Address.AddressBuilder create() {
        return new Address.AddressBuilder();
    }

    public static class AddressBuilder {

        private String streetType;
        private String streetName;
        private String streetExtendedData;
        private String postalCode;
        private String location;
        private String municipality;
        private String province;

        public Address.AddressBuilder withStreetType(String streetType) {
            this.streetType = streetType;
            return this;
        }

        public Address.AddressBuilder withStreetName(String streetName) {
            this.streetName = streetName;
            return this;
        }

        public Address.AddressBuilder withStreetExtendedData(String streetExtendedData) {
            this.streetExtendedData = streetExtendedData;
            return this;
        }

        public Address.AddressBuilder withPostalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public Address.AddressBuilder withLocation(String location) {
            this.location = location;
            return this;
        }

        public Address.AddressBuilder withMunicipality(String municipality) {
            this.municipality = municipality;
            return this;
        }

        public Address.AddressBuilder withProvince(String province) {
            this.province = province;
            return this;
        }

        public Address build() {
            return new Address(this.streetType, this.streetName, this.streetExtendedData, this.postalCode, this.location, this.municipality, this.province);
        }
    }

    @Override
    public String toString(){
        String locationToPrint = null;
        if(location != null){
            locationToPrint = getLocation().concat("   ");
        }

        return getStreetExtendedData().concat("  ")
                .concat(locationToPrint)
                .concat(getPostalCode()).concat("  ")
                .concat(getMunicipality());
    }
}
