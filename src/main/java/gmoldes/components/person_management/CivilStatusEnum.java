package gmoldes.components.person_management;

public enum CivilStatusEnum {

    No_disponible("No disponible"),
    Soltero("Soltero"),
    Soltera("Soltera"),
    Casado("Casado"),
    Casada("Casada"),
    Separado_judicialmente("Separado judicialmente"),
    Separada_judicialmente("Separada judicialmente"),
    Divorciado("Divorciado"),
    Divorciada("Divorciada"),
    Viudo("Viudo"),
    Viuda("Viuda"),
    Pareja_de_hecho("Pareja de hecho");

    private final String value;

    CivilStatusEnum(String value) {
        this.value = value.replace(" ","_");
    }

    public String getValue() {
        return value.replace(" ", "_");
    }

    public String toString(){
        return value.replace("_"," ");
    }
}
