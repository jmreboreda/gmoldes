package gmoldes.components.consultation_contract.components;

import gmoldes.ApplicationConstants;
import gmoldes.components.ViewLoader;
import gmoldes.components.contract_documentation_control.events.ContractSelectedEvent;
import gmoldes.components.contract_documentation_control.events.SelectClientEmployerEvent;
import gmoldes.components.contract_documentation_control.events.SelectEmployerEmployeeEvent;
import gmoldes.domain.client.dto.ClientDTO;
import gmoldes.domain.person.dto.PersonDTO;
import gmoldes.domain.timerecord.dto.TimeRecordClientDTO;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ConsultationContractSelector extends AnchorPane {

    private static final String CONSULTATION_CONTRACT_SELECTOR_FXML = "/fxml/consultation_contract/consultation_contract_selector.fxml";
    private static final String FONT_NAME = "Noto Sans";


    private Parent parent;
    private Stage stage;

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT);

    private EventHandler<MouseEvent> onActiveClientsOnlyEventHandler;
    private EventHandler<MouseEvent> onContractInForceOnlyEventEventHandler;
    private EventHandler<MouseEvent> onAllContractEventEventHandler;
    private EventHandler<SelectClientEmployerEvent> selectClientEmployerEventEventHandler;
    private EventHandler<SelectEmployerEmployeeEvent> selectEmployerEmployeeEventEventHandler;
    private EventHandler<ContractSelectedEvent> contractSelectedEventEventHandler;

    @FXML
    private ToggleGroup contractActiveOrNot;
    @FXML
    private CheckBox activeClientsOnly;
    @FXML
    private RadioButton contractInForceOnly;
    @FXML
    private DatePicker inForceDate;
    @FXML
    private RadioButton allContract;
    @FXML
    private ComboBox<ClientDTO> clientSelector;
    @FXML
    private ComboBox<PersonDTO> employeeSelector;
    @FXML
    private ComboBox<Integer> contractSelector;

    public ConsultationContractSelector() {
        this.parent = ViewLoader.load(this, CONSULTATION_CONTRACT_SELECTOR_FXML);

    }

    public void initialize(){

        clientSelector.setButtonCell(new ListCell(){

            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font (FONT_NAME, 12));
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font (FONT_NAME, 12));
                    setTextFill(Color.BLUE);
                    setText(item.toString());
                }
            }

        });

        clientSelector.setCellFactory(
                new Callback<ListView<ClientDTO>, ListCell<ClientDTO>>() {
                    @Override public ListCell<ClientDTO> call(ListView<ClientDTO> param) {
                        final ListCell<ClientDTO> cell = new ListCell<ClientDTO>() {
                            @Override public void updateItem(ClientDTO item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font (FONT_NAME, 12));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }

                });

        employeeSelector.setButtonCell(new ListCell(){

            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font (FONT_NAME, 12));
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font (FONT_NAME, 12));
                    setTextFill(Color.BLUE);
                    setText(item.toString());
                }
            }

        });

        employeeSelector.setCellFactory(
                new Callback<ListView<PersonDTO>, ListCell<PersonDTO>>() {
                    @Override public ListCell<PersonDTO> call(ListView<PersonDTO> param) {
                        final ListCell<PersonDTO> cell = new ListCell<PersonDTO>() {
                            @Override public void updateItem(PersonDTO item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font (FONT_NAME, 12));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }

                });

        contractSelector.setButtonCell(new ListCell(){

            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font (FONT_NAME, 12));
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font (FONT_NAME, 12));
                    setTextFill(Color.BLUE);
                    setText(item.toString());
                }
            }

        });

        contractSelector.setCellFactory(
                new Callback<ListView<Integer>, ListCell<Integer>>() {
                    @Override public ListCell<Integer> call(ListView<Integer> param) {
                        final ListCell<Integer> cell = new ListCell<Integer>() {
                            @Override public void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font (FONT_NAME, 12));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }

                });

        activeClientsOnly.setOnMouseClicked(this::onActiveClientsOnly);
        contractInForceOnly.setOnMouseClicked(this::onContractInForceOnly);
        inForceDate.setOnMouseClicked(this::onInForceDate);
        allContract.setOnMouseClicked(this::onAllContract);

        clientSelector.setOnAction(this::onClientSelectorChange);
        employeeSelector.setOnAction(this::onEmployeeSelectorChange);
        contractSelector.setOnAction(this::onContractSelectorChange);

        inForceDate.setConverter(new LocalDateStringConverter(dateFormatter, null));
        inForceDate.setValue(LocalDate.now());

        allContract.setTooltip(new Tooltip("Contratos vencidos, en vigor y futuros"));

    }

    public CheckBox getActiveClientsOnly() {
        return activeClientsOnly;
    }

    public RadioButton getContractInForceOnly() {
        return contractInForceOnly;
    }

    public DatePicker getInForceDate() {
        return inForceDate;
    }

    public RadioButton getAllContract() {
        return allContract;
    }

    public ComboBox<ClientDTO> getClientSelector() {
        return clientSelector;
    }

    public ComboBox<PersonDTO> getEmployeeSelector() {
        return employeeSelector;
    }

    public ComboBox<Integer> getContractSelector() {
        return contractSelector;
    }

    private void onActiveClientsOnly(MouseEvent event){
        onActiveClientsOnlyEventHandler.handle(event);
    }

    private void onContractInForceOnly(MouseEvent event){
        onContractInForceOnlyEventEventHandler.handle(event);
    }

    private void onAllContract(MouseEvent event){
        onAllContractEventEventHandler.handle(event);
    }

    private void onClientSelectorChange(ActionEvent event){
        if(getClientSelector().getValue() == null){

            return;
        }

        SelectClientEmployerEvent clientEmployerEvent = new SelectClientEmployerEvent(getClientSelector().getValue());
        selectClientEmployerEventEventHandler.handle(clientEmployerEvent);
    }

    private void onEmployeeSelectorChange(ActionEvent event){
        if(getEmployeeSelector().getValue() == null){

            return;
        }

    SelectEmployerEmployeeEvent employerEmployeeEvent = new SelectEmployerEmployeeEvent(getClientSelector().getValue(), getEmployeeSelector().getValue());
    selectEmployerEmployeeEventEventHandler.handle(employerEmployeeEvent);
    }

    private void onInForceDate(MouseEvent event){
        onContractInForceOnly(event);
    }

    private void onContractSelectorChange(ActionEvent event){
        if(contractSelector.getSelectionModel().getSelectedItem() == null){

            return;
        }

        contractSelectedEventEventHandler.handle(new ContractSelectedEvent(contractSelector.getValue()));
    }

    public void loadClientSelector(ObservableList<ClientDTO> clientDTOOL){
        clientSelector.setItems(clientDTOOL);
    }

    public void setOnActiveClientsOnly(EventHandler<MouseEvent> onActiveClientsOnlyEventHandler){
        this.onActiveClientsOnlyEventHandler = onActiveClientsOnlyEventHandler;
    }

    public void setOnContractInForceOnly(EventHandler<MouseEvent> event){
        this.onContractInForceOnlyEventEventHandler = event;
    }

    public void setOnAllContract(EventHandler<MouseEvent> event){
        this.onAllContractEventEventHandler = event;
    }
    public void setOnClientSelectorChange(EventHandler<SelectClientEmployerEvent> selectClientEmployerEventEventHandler){
        this.selectClientEmployerEventEventHandler = selectClientEmployerEventEventHandler;
    }

    public void setOnEmployeeSelectorChange(EventHandler<SelectEmployerEmployeeEvent> selectEmployerEmployeeEventEventHandler){
        this.selectEmployerEmployeeEventEventHandler = selectEmployerEmployeeEventEventHandler;
    }

    public void setOnContractSelectorChange(EventHandler<ContractSelectedEvent> contractSelectedEventEventHandler){
        this.contractSelectedEventEventHandler = contractSelectedEventEventHandler;
    }
}
